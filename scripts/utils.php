<?php

function installProduct($product, $homeDir)
{
    echo("\n\n");
    
    printAndUnderline("Downloading " . $product['shortname']);
    
	//download product
	$hashfile = "/tmp/" . md5($product['downloadurl']);
    if (!file_exists($hashfile)) {
        runCommand("curl " . $product['downloadurl'] . " -o " . $hashfile);
    }
    runCommand("cp " . $hashfile . " " . $product['filename']);

	//unzip product
    printAndUnderline("Expanding " . $product['shortname']);
    if (strpos($product['downloadurl'],'.tar.gz') > 0) {
        $command = "tar -zxf " . $product['filename'];
    } else {
        $command = "unzip -q " . $product['filename'] . ' -x "__MACOSX*"';
    }
    runCommand($command);
    
    //moving product to the right location
	printAndUnderline("Relocating " . $product['shortname']);
    
    if (file_exists($homeDir . "/inst/" . strtolower($product['shortname']))) {
        exec("rm -r " . $homeDir . "/inst/" . strtolower($product['shortname']));
    }   
    
    runCommand("mv " . $product['dirname'] ." ". $homeDir . "/inst/" . strtolower($product['shortname']));
    runCommand("rm " . $product['filename']); 
	
	//installing plugins
	printAndUnderline("Installing addons for " . $product['shortname']);
	installPlugins($product['plugindir'], $product['plugins'],$homeDir);
	
}

function installPlugins($pluginDir, $plugins,$homeDir) {
	$installDir = getcwd();
	mkdir($homeDir. $pluginDir, 0755, true);
	
	foreach ($plugins as $plugin) {
		mkdir($installDir . '/plugin');
		chdir($installDir . '/plugin');
		runCommand("curl -O -J -L " . $plugin);
		if (sizeof(glob("*.obr"))) {
			runCommand("unzip *.obr");
			runCommand("cp dependencies/*.jar " . $homeDir. $pluginDir);
		}
		runCommand("cp *.jar " . $homeDir. $pluginDir);
		chdir('..');
		runCommand("rm -r ".$installDir."/plugin");
	}
}

function runCommand($command) {
	echo("Running: " . $command . "\n");
	$proc = popen($command, "r");
    while (!feof($proc))
    {
        echo fread($proc, 4096);
        @ flush();
    }
}

function printAndUnderline($instring) {
    echo("\n");
    echo(str_repeat("-", 80) . "\n");
    echo("| ". $instring . str_repeat(" ", 77-strlen($instring)) . "|\n");
    echo(str_repeat("-", 80) . "\n\n");
}

function installCrowd($product, $homeDir) {
    
    installProduct($product, $homeDir);

    //Set the home directory
    $config = file_get_contents($homeDir . "/inst/crowd/crowd-webapp/WEB-INF/classes/crowd-init.properties");
    file_put_contents($homeDir . "/inst/crowd/crowd-webapp/WEB-INF/classes/crowd-init.properties", 
        "crowd.home=" . $homeDir. "/data/crowd\n" . $config);
    
    //set the default port from 8095 to 2430
    $config = file_get_contents($homeDir . "/inst/crowd/build.properties");
    file_put_contents($homeDir . "/inst/crowd/build.properties", 
        str_replace(8095, 2430, $config));
                      
    //run build.sh
    $installDir = getcwd();
    chdir($homeDir. "/inst/crowd/");
    runCommand("sh ./build.sh");
    chdir($installDir);
}

function installConfluence($product, $homeDir) {
    
    installProduct($product, $homeDir);
    $config = file_get_contents($homeDir . "/inst/confluence/confluence/WEB-INF/classes/confluence-init.properties");
    file_put_contents($homeDir . "/inst/confluence/confluence/WEB-INF/classes/confluence-init.properties", 
        "confluence.home=" . $homeDir. "/data/confluence\n" . $config);
    
}

function installBamboo($product, $homeDir) {
    
    installProduct($product, $homeDir);
    $config = file_get_contents($homeDir . "/inst/bamboo/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties");
    file_put_contents($homeDir . "/inst/bamboo/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties", 
        "bamboo.home=" . $homeDir. "/data/bamboo\n" . $config);
    
}

function installJIRA($product, $homeDir) {
    
    installProduct($product, $homeDir);
    
}

function installStash($product, $homeDir) {
    
    installProduct($product, $homeDir);
    
}

function installFeCru($product, $homeDir) {
    
    installProduct($product, $homeDir);
    
}

function installSourceTree($product, $homeDir) {
    runCommand("curl " . $product['downloadurl'] . " -o " . $product['filename']);
    runCommand("hdiutil mount " . $product['filename']);
    //if (file_exists($product['location'])) {
    //    exec("sudo rm -r " . $product['location']);
    //}   
    
    runCommand("sudo cp -R /Volumes/" . $product['dirname'] ." ". $product['location']);
    runCommand("rm " . $product['filename']); 
    runCommand("hdiutil unmount /Volumes/" . $product['dirname']);

}
?>

