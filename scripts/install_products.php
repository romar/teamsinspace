<?php
require_once('utils.php');

$homeDir = dirname(getcwd());


if (!file_exists($homeDir . "/inst/")) {
    mkdir($homeDir . "/inst/", 0777, true);
}

$productxml = simplexml_load_file("products.xml");

//build product array
$products = [];

foreach($productxml as $product) {
    foreach ($product->children() as $key => $value) {    
		$products[strtolower($product->shortname)][$key] = str_replace("^version^", (string)$product->version, (string)$product->$key);  
    }
	if (isset($product->plugins->plugin)) {
		$products[strtolower($product->shortname)]['plugins'] = array();
		foreach ($product->plugins->plugin as $plugin) {    
			$products[strtolower($product->shortname)]['plugins'][] = (string)$plugin;
		}
    }
}

switch (strtolower($argv[1])) {
    case 'crowd':
        installCrowd($products['crowd'], $homeDir);
        break;
    case 'jira':
        installJIRA($products['jira'], $homeDir);
        break;
    case 'confluence':
        installConfluence($products['confluence'], $homeDir);
        break;
    case 'bamboo':
        installBamboo($products['bamboo'], $homeDir);
        break;
    case 'stash':
        installStash($products['stash'], $homeDir);
        break;
    case 'fecru':
        installFeCru($products['fecru'], $homeDir);
        break;
    case 'sourcetree':
        installSourceTree($products['sourcetree'], $homeDir);
        break;
    case 'all':
        installCrowd($products['crowd'], $homeDir);
        installJIRA($products['jira'], $homeDir);
        installConfluence($products['confluence'], $homeDir);
        installBamboo($products['bamboo'], $homeDir);
        installStash($products['stash'], $homeDir);
        installFeCru($products['fecru'], $homeDir);
        break;
}
?>
