<?php

/*
 * This script allows searching for and updating date string in a file.
 *
 * Originally used to update the entities.xml and activeobjects.xml, we now run it directly against the hsql db dump file (jiradb.script)
 * in order to shift dates around in time
 */

// The obvious center of the world
date_default_timezone_set('America/Los_Angeles');

//
// Actual stuff we execute against the file
//

/**
 * Should the given line be ignored
 */
function shouldLineBeIgnored($line, &$context)
{
    $contentToIgnore = array(
        'com.atlassian.servicedesk:Upgrade.History',
        'com.atlassian.servicedesk:Run.History',
        'latestUpgradeTaskRun'
    );
    foreach($contentToIgnore as $snippet)
    {
        if (strpos($line, $snippet) !== false)
        {
            return true;
        }
    }

    return false;
}


/**
 * Finds the min/max date and stores it in context['minMaxRange']
 */
function findMinMaxDate($date, $dateType, &$context)
{
    $minMaxRange = &$context['minMaxRange'];
    if ($date < getStart($minMaxRange))
    {
        setStart($minMaxRange, $date);
    }
    if ($date > getEnd($minMaxRange))
    {
        setEnd($minMaxRange, $date);
    }

    // return date as-is
    return $date;
}

/**
 * Finds the max date range contained in a file
 */
function findMaxRange($file)
{
    $minMaxRange = asRange(
        new DateTime('2036-01-01'),
        new DateTime('1970-01-01')
    );
    $context = array(
        'validRange' => getDefaultValidRange(),
//        'validRange' => asRange(
//            new DateTime("2013-06-01"),
//            new DateTime("2013-12-31")
//        ),
        'dateFunction' => "findMinMaxDate",
        'minMaxRange' => &$minMaxRange
    );

    //Calling `visitAllLines` will set $minMaxRange
    $lines = visitAllLines($file['lines'], $context);
    echo 'Min Date: '.getStart($minMaxRange)->format('Y-m-d H:i:s')."\n".
        'Max Date: '.getEnd($minMaxRange)->format('Y-m-d H:i:s')."\n";
    return $minMaxRange;
}

/**
 * Shifts the date according to the context and returns it
 */
function shiftDate($date, $dateType, &$context)
{
    //Shift period in full days, given in context
    $daysToShift = $context["daysToShift"];

    if($daysToShift >= 0)
    {
        //E.g. "P42D" will shift the date forward by 42 days
        $shiftInterval = new DateInterval("P".$daysToShift."D");
        $date->add($shiftInterval);
    }
    else
    {
        //E.g. "P42D" will shift the date forward by 42 days
        $shiftInterval = new DateInterval("P".(-1*$daysToShift)."D");
        $date->sub($shiftInterval);
    }

    return $date;
}

// Old logic
function adjust_date($date)
{

    //calculate percentage through
    //var_dump($date);
    $interval = $date->getTimestamp()-$GLOBALS['early_date']->getTimestamp();
    $interval = intval($interval/24/3600);

    $percentage_interval = 1-($interval/$GLOBALS['span']);

    return DateTime::createFromFormat("U", time()-intval($GLOBALS['compression_interval']*$percentage_interval*24*3600));
}

/**
 * Shifts all dates in the given file by $daysToShift and stores them to the path
 * specified by $outFilePath.
 */
function shiftDates($file, $outFilePath, $daysToShift)
{
    $context = array(
        'validRange'        => getDefaultValidRange(),
        'dateFunction'      => 'shiftDate',
        'daysToShift'       => $daysToShift
    );
    $lines = visitAllLines($file['lines'], $context);
    $file['lines'] = $lines;
    $file['path'] = $outFilePath;
    storeFile($file);
}

function saveDatesPermanent($file, $outFilePath)
{
    $datePattern = "/-- [0-9]{4}-[0-9]{2}-[0-9]{2}/";
    $dateHeader = '-- ' . (new DateTime())->format('Y-m-d');
    if(preg_match($datePattern, $file['lines'][0]) === 1)
    {
        $file['lines'][0] = $dateHeader;
    }
    else
    {
        array_unshift($file['lines'] , $dateHeader);
    }

    $file['path'] = $outFilePath;
    storeFile($file);
}

//
// Range stuff
//

/**
 * Get the start of a range
 */
function getStart($range)
{
    return $range['start'];
}

/**
 * Sets the start of a range
 */
function setStart(&$range, $start)
{
    $range['start'] = $start;
}

/**
 * Get the end of a range
 */
function getEnd($range)
{
    return $range['end'];
}

/**
 * Sets the end of a range
 */
function setEnd(&$range, $end)
{
    $range['end'] = $end;
}

/**
 * Wraps start and end into a range
 */
function asRange($start, $end)
{
    return array(
        'start' => $start,
        'end' => $end
    );
}

/**
 * Is the passed date in the target time range?
 */
function isInRange($date, $range)
{
    $timestamp = $date->getTimestamp();
    return getStart($range)->getTimestamp() <= $timestamp && $timestamp <= getEnd($range)->getTimestamp();
}

/**
 * Get the default valid range
 */
function getDefaultValidRange()
{
    $current_date = new DateTime();
    $start = new DateTime();
    $start->setTimestamp($current_date->getTimestamp()-365*3600*24);        //1 year ago
    $end = new DateTime();
    $end->setTimestamp($current_date->getTimestamp()+365*3600*24);          //1 year in the future
    $validRange = asRange(
        $start,
        $end
    );
    return $validRange;
}


//
// File io
//

/**
 * Loads a file
 */
function loadFile($path)
{
    $buffer = file_get_contents($path);
    $lines = preg_split("/((\r?\n)|(\r\n?))/", $buffer);

    return array(
        'buffer' => $buffer,
        'lines' => $lines,
        'path' => $path
    );
}

/**
 * Stores a previously opened file using the possibly altered lines
 */
function storeFile(&$file)
{
    // put the lines back together into a buffer
    $buffer = implode(PHP_EOL, $file['lines']);
    file_put_contents($file['path'], $buffer);
    $file['buffer'] = $buffer;
    echo 'File stored to ' . realpath($file['path']) . "\n";
}


//
// Date parsing and processing
//

/**
 * Should the date be processed?
 *
 * Uses validRange inside the context
 */
function shouldProcessDate($date, $context)
{
    return isInRange($date, $context['validRange']);
}

/**
 * Processes all y-m-d dates found in a line
 *
 * Returns the altered line
 */
function visitYMDDates($line, &$context)
{
    $offset = 0;
    $pattern = "|([0-9]{4}-[0-9]{2}-[0-9]{2})|";
    $matches = array();
    while(preg_match($pattern, $line, $matches, PREG_OFFSET_CAPTURE, $offset))
    {
        // position of matched element
        $match_start = $matches[0][1];
        $match = $matches[0][0];
        $match_length = strlen($match);

        // convert to date
        $date = new DateTime($match);

        // do we want to process this date?
        if (!shouldProcessDate($date, $context))
        {
            $offset = $match_start + $match_length;
            continue;
        }

        // update date
        $updatedDate = $context['dateFunction']($date, 'ymd', $context);
        $updatedDateString = $updatedDate->format('Y-m-d');

        // put together result
        $prefixAndReplacement = substr($line, 0, $match_start) . $updatedDateString;
        $line = $prefixAndReplacement . substr($line, $match_start+$match_length);

        // Update offset to the end of the match
        $offset = strlen($prefixAndReplacement);
    }
    return $line;
}

/**
 * Processes all millis since epoque dates found in a line
 * Returns the altered line
 */
function visitMillisDates($line, &$context)
{
    $offset = 0;
    $pattern = "/(?:[:,'])([0-9]{13})/";  //Ensure we don't match some random identifier string
                                            //(1st character must be one of :,')
    $matches = array();
    while(preg_match($pattern, $line, $matches, PREG_OFFSET_CAPTURE, $offset))
    {
        // position of matched element
        $match_start = $matches[1][1];
        $match = $matches[1][0];
        $match_length = strlen($match);

        // echo "Match: " . $match;

        // convert to date
        $date = DateTime::createFromFormat("U", round(intval($match)/1000));

        // do we want to process this date?
        if (!shouldProcessDate($date, $context))
        {
            $offset = $match_start + $match_length;
            continue;
        }

        // update date
        $updatedDate = $context['dateFunction']($date, 'ymd', $context);
        $updatedDateString = '' . $updatedDate->getTimestamp()*1000;

//        echo ", replaced with: " . $updatedDateString . "\n";

        // put together result
        $prefixAndReplacement = substr($line, 0, $match_start) . $updatedDateString;
        $line = $prefixAndReplacement . substr($line, $match_start+$match_length);

        // Update offset to the end of the match
        $offset = strlen($prefixAndReplacement);
    }
    return $line;
}

/**
 * What date formats should check for in a given line?
 *
 * This function allows us to filter lines or only apply certain types of dates
 */
function getApplicableDateFormats($line, &$context)
{
    //Both cases do the same thing?
    if (shouldLineBeIgnored($line, $context))
    {
//        echo 'Ignoring: '.$line."\n";
        //These were both true before? (exactly the same as else case)
        return array(
            'ymd' => false,
            'millis' => false
        );
    }
    else
    {
        return array(
            'ymd' => true,
            'millis' => true
        );
    }
}

/**
 * Applies date functions over a line.
 */
function visitLine($line, &$context)
{
    // what formats do we replace in given line?
    $formats = getApplicableDateFormats($line, $context);

    if ($formats['ymd'])
    {
        $line = visitYMDDates($line, $context);
    }

    if ($formats['millis'])
    {
        $line = visitMillisDates($line, $context);
    }

    return $line;
}

/**
 * Visits all lines, applying context to it. Returns the resulting lines
 */
function visitAllLines($lines, &$context)
{
    $res = array();
    foreach ($lines as $index => $line)
    {
        $res[$index] = visitLine($line, $context);
    }
    return $res;
}


//
// main
//

/**
 * Main function executed by this script
 */
function main($argv)
{
    $function = $argv[1];
    $inFilePath = realpath($argv[2]);
    $outFilePath = $argv[3];

    echo 'Processing '.$inFilePath.' with '.$function."\n";

    // load the file
    $inFile = loadFile($inFilePath);

    switch($function)
    {
        case 'findMaxRange':
            findMaxRange($inFile);
            break;
        case 'shiftDatesToPresent':
            $datePattern = "/-- [0-9]{4}-[0-9]{2}-[0-9]{2}/";
            if(preg_match($datePattern, $inFile['lines'][0]) !== 1)
            {
                echo "Error: Expected date (current date of data-set) in the form '-- YYYY-MM-DD' on the first line of ";
                echo "'" . $inFilePath . "'\n";
                exit(1);
            }

            //The `NOW` date in the dataset, from the first line
            $nowDateStr = str_replace("-- ", "", $inFile['lines'][0]);
            $datasetNowDate = new DateTime($nowDateStr);
            $dateDiff = $datasetNowDate->diff(new DateTime());
            $daysDiff = $dateDiff->days;

            //Time interval is negative, need to shift dates backwards
            if($dateDiff->invert == 1)
            {
                $daysDiff *= -1;
            }
            echo "Shifting dates " . ($daysDiff > 0 ? "forward" : "backward") . " by " . abs($daysDiff) . " days\n";
            shiftDates($inFile, $outFilePath, $daysDiff);
            break;
        case 'saveDatesPermanent':
            echo "Saving dates to: " . $outFilePath . "\n";
            saveDatesPermanent($inFile, $outFilePath);
            break;
        default:
            echo "Unknown function.\n";
    }
}

// run the script
main($argv);
