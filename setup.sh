#!/bin/sh

# Base directory
export TIS_BASE=/opt/atlassian

#Installation Directories
export CROWD_INST=${TIS_BASE}/inst/crowd
export JIRA_INST=${TIS_BASE}/inst/jira
export CONY_INST=${TIS_BASE}/inst/confluence
export STASH_INST=${TIS_BASE}/inst/stash
export BAMBOO_INST=${TIS_BASE}/inst/bamboo
export FECRU_INST=${TIS_BASE}/inst/fecru
#export JAVA_HOME=${TIS_BASE}/jira/inst/jre

# Data dierctories
export CROWD_HOME=${TIS_BASE}/data/crowd
export JIRA_HOME=${TIS_BASE}/data/jira
export CONY_HOME=${TIS_BASE}/data/confluence
export STASH_HOME=${TIS_BASE}/data/stash
export BAMBOO_HOME=${TIS_BASE}/data/bamboo
export FISHEYE_INST=${TIS_BASE}/data/fecru

# Executables
export PATH=$JAVA_HOME/bin:$CROWD_INST:$JIRA_INST/bin:$CONY_INST/bin:$STASH_INST/bin:$BAMBOO_INST/bin:$FECRU_INST/bin:$PATH

# Commands

# Start Crowd -- start-crowd
alias start-crowd=start_crowd.sh
# Stop Crowd -- stop-crowd
alias stop-crowd=stop_crowd.sh

# Start JIRA -- start-jira
alias start-jira=start-jira.sh
# Stop JIRA -- stop-jira
alias stop-jira=stop-jira.sh

# Start Stash -- start-stash
alias start-stash=start-stash.sh
# Stop Stash -- stop-stash
alias stop-stash=stop-stash.sh

alias start-confluence=start-confluence.sh
alias stop-confluence=stop-confluence.sh

alias start-fecru=start.sh
alias stop-fecru=stop.sh

alias start-bamboo=start-bamboo.sh
alias stop-bamboo=stop-bamboo.sh





alias tail-crowd-log='tail -f $CROWD_HOME/logs/atlassian-crowd.log'
alias less-crowd-log='less $CROWD_HOME/logs/atlassian-crowd.log'

alias tail-jira-log='tail -f $JIRA_HOME/log/atlassian-jira.log'
alias less-jira-log='less $JIRA_HOME/log/atlassian-jira.log'

alias tail-confluence-log='tail -f $CONY_HOME/logs/atlassian-confluence.log'
alias less-confluence-log='less $CONY_HOME/logs/atlassian-confluence.log'

alias tail-stash-log='tail -f $STASH_HOME/log/atlassian-stash.log'
alias less-stash-log='less $STASH_HOME/log/atlassian-stash.log'

alias tail-fecru-log='tail -f $FISHEYE_INST/var/log/fisheye.out'
alias less-fecru-log='less $FISHEYE_INST/var/log/fisheye.out'

alias tail-bamboo-log='tail -f $BAMBOO_HOME/logs/atlassian-bamboo.log'
alias less-bamboo-log='less $BAMBOO_HOME/logs/atlassian-bamboo.log'

function tis-check() {
    echo "Checking for processes running from ${TIS_BASE}..." 1>&2
    pgrep -lf ${TIS_BASE}
    retval=$?
    if [ $retval -eq 0 ] ; then
        echo "1 or more processes running" 1>&2
    else
        echo "No processes running" 1>&2
    fi
    return $retval
}

function tis-stop() {
    if ! tis-check; then
        echo "Already stopped"
        return 1
    fi
    echo "Stopping all..." 1>&2
    stop-jira
    stop-confluence
    stop-stash
    stop-bamboo
    stop-fecru
    stop-crowd
    sleep 5
    pkill -f /opt/atlassian/inst/crowd
    sleep 5

    tis-check
    retval=$?
    if [ $retval -eq 0 ] ; then
        echo "Something is still running. Trying again usually works, otherwise you'll need to kill the remaining processes above manually" 1>&2
    fi
    return $retval
}

function tis-start() {
    tis-notify "Starting all..."
    start-crowd
    start-jira
    start-confluence
    start-stash
    start-bamboo
    start-fecru
    sleep 5

    echo "$(tis-check | wc -l) processes running" 1>&2
}

function branch-check() {
    if [ `cd $TIS_BASE; git rev-parse --abbrev-ref HEAD` != $1 ]; then
        echo "WARNING: You are on a different branch! `cd $TIS_BASE; git rev-parse --abbrev-ref HEAD` != $1"
        echo
    fi
    if ! (cd $TIS_BASE; git remote -v) | grep -q atlassian/teams-in-space; then
        echo "WARNING: Your git remote doesn't appear to be pointing at the correct repo"
        (cd $TIS_BASE; git remote -v)
        echo "To fix:"
        echo "cd $TIS_BASE; git remote set-url origin git@bitbucket.org:atlassian/teams-in-space-demo.git"
        for i in `seq 25`; do
            echo "WARNING!!! ^^^^ LOOK UP!!!"
        done
    fi
}

function tis-commit() {
    tis-stop && echo "Couldn't stop all processes, bailing out" && return

    branch=${1:-master}
    branch-check "$branch"

    echo "Upstream safety check - there shouldn't be anything between these lines."
    echo "---------------------------------------------------------------"
    (cd $TIS_BASE; git fetch && git log HEAD..origin/master --oneline)
    echo "---------------------------------------------------------------"
    echo
    (cd $TIS_BASE; git status)
    echo
    echo "Check the git status output above and then type the following when you are satisfied with the changes..."
    echo "git add --all data"
    echo "git commit"
    echo "git push"
    echo
    echo "When you are done, test that your changes work through a reset by typing:"
    echo "tis-reset"
}

function tis-smoketest() {
    echo "Running smoke tests..." 1>&2
    while read tisuser url match; do
        output_file=/tmp/test_results.txt
        echo -n "Checking ${tisuser}@${url} for ${match}..."
        curl -s "http://${tisuser}:Charlie!@${url}" -H 'Accept: application/json, text/javascript, */*; q=0.01' > ${output_file}
        grep -q "${match}" ${output_file}
        if [ $? -eq 0 ]; then
            echo " ok"
        else
            echo " failed."
            #ls -al ${output_file}
            #break
        fi
    done < <(grep "$1" "${TIS_BASE}/smoketest_data.txt")
}

function tis-clean() {
    tis-notify "Cleaning..."
    if [ "$1" = "--hard" ]; then
        shift
        hard_reset=1
    else
        unset hard_reset
    fi
    branch=${1:-master}
    branch-check "$branch"

    if [ -n "$hard_reset" ]; then
        (cd $TIS_BASE; git checkout "$branch" && git reset --hard "$branch" && git clean -d -x -f -f)
    else
        (cd $TIS_BASE; git checkout "$branch" && git reset --hard "$branch" && git clean -f data)
    fi

    (cd $TIS_BASE; git pull origin)
}

function tis-install() {
    tis-notify "Installing products..."
    (cd $TIS_BASE/scripts; sh installproducts.sh)
}

function tis-reindex() {
    tis-notify "Reindexing..."
    echo "Confluence:"
    curl 'http://localhost:8090/rest/prototype/1/index/reindex' --user admin:Charlie! -X POST
    echo
    echo "JIRA:"
    curl 'http://localhost:8080/rest/api/2/reindex?type=FOREGROUND' --user admin:Charlie! -X POST
    echo
    echo "Bamboo:"
    curl 'http://localhost:8085/rest/api/latest/reindex' --user admin:Charlie! -X POST
    echo
}

function tis-reset() {
    tis-stop && echo "Couldn't stop all processes, bailing out" && return
    echo "Uncommitted changes:"
    (cd $TIS_BASE; git status -sb)
    read -p "Are you sure you want to continue? " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        tis-notify "Resetting..."
        tis-clean $@
        if [ "$1" = "--hard" ]; then
            tis-install
        fi

        tis-notify "=== Executing movetopresent.sh date script to shift JIRA through time! ==="
        cd $TIS_BASE/scripts
        ./movetopresent.sh
        tis-notify "=== Date shifting DONE  ==="
        cd $TIS_BASE

        tis-start
        tis-notify "Sleeping for 60 seconds to allow products to start up before re-indexing..."
        sleep 60
        tis-reindex
        tis-smoketest
        tis-notify "Reset complete"
    fi
}

function tis-notify() {
    echo "$1" 1>&2
    if [ -n "$1" -a -n "${hc_token}" -a -n "${hc_room}" ]; then
        curl "https://api.hipchat.com/v2/room/${hc_room}/notification?auth_token=${hc_token}" -H "Content-Type: application/json" -d "{\"message\":\"${hc_prefix}$1\"}"
    fi
}

echo "Teams in Space is set up!" 1>&2
